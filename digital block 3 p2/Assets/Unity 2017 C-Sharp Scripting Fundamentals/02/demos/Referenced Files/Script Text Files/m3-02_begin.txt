using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript_C : MonoBehaviour
{
    public Collider playerCollider;
    public Transform playerTransform;

	// Use this for initialization
	void Start ()
    {
        playerCollider = GetComponent<Collider>();
        playerTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

