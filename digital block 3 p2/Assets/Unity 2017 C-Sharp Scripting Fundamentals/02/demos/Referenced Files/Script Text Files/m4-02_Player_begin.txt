using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private GameObject enemy;
    private Enemy enemyScript;
	// Use this for initialization
	void Start ()
    {
        enemy = GameObject.Find("Enemy");
        enemyScript = enemy.GetComponent<Enemy>();

	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyUp("space"))
        {
            enemyScript.enemyHealth--;
        }
	}
}

